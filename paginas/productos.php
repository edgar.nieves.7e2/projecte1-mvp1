<html>

<head>
    <meta charset="UTF-8">
    <title>PRODUCTOS</title>
    <meta name="viewport" content=" initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../css/productos.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div class="rrss">
        <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
        <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
        <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
        <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
    </div>
    <header>
        <a href="../index.html"><img class="logo" src="../img/logo.png"></a>
        <nav>
            <ul>
                <li><a href="#foot">Contacto</a></li>
                <li><a href="../index.html#not">Noticias</a></li>
            </ul>
        </nav>
        <div class="topnav" id="myTopnav">
            <a href="productos.php" class="active">PRODUCTOS</a>
            <a href="#foot">CONTACTO</a>
            <a href="../index.html#not">NOTICIAS</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
              <i class="fa fa-bars"></i>
            </a>
        </div>

        <script>
            function myFunction() {
              var x = document.getElementById("myTopnav");
              if (x.className === "topnav") {
                x.className += " responsive";
              } else {
                x.className = "topnav";
              }
            }
        </script>
    </header>
    <?php
    $page = 1;
    session_start();
    include('prod.php');
    include('cesta.php');
    ?>

    <h1>Productos</h1>
    <div class="listas" >
        <h4>-Categorias: </h4>
        <br>
        <br>
        <?php
        $cat = [];
        foreach($elMeuCataleg->productes as $prod){
            $cat[] = $prod->categoria;
        }
        $cat2 = array_unique($cat);
        $cat3 = [];
        foreach($cat2 as $cat){
            $cat3[] = $cat;
        }
        foreach($cat3 as $cat){
            echo "<p><a href=#" . $cat . ">" . $cat . "</a></p><br>";
        }
        ?>
    </div>
    <?php
    $cat = [];
    foreach($elMeuCataleg->productes as $prod){
        $cat[] = $prod->categoria;
    }
    $cat2 = array_unique($cat);
    $cat3 = [];
    foreach($cat2 as $cat){
        $cat3[] = $cat;
    }

    foreach($cat3 as $cat){
        echo "<h2 class='prod' id='" . $cat . "'>" . $cat . "</h2>";
        foreach($elMeuCataleg->productes as $prod){
            if($prod->categoria == $cat){
                echo '<a href="producto.php?id=' . $prod->id . '">';
                echo '<div class="articulos">';
                echo '<article>';
                echo '<img style="width:17%; float: left; margin-right: 20px;" src="' . $prod->fotos[0] . '"/>';
                echo '<h6>' . $prod->categoria . '</h6>';
                echo '<h4>' . $prod->titol . '</h4>';
                echo '<p>' . $prod->descripció . '</p>';
                echo '<img style="width: 100px" class="estrella" src="../img/estrella.png">';
                echo '<p>' . $prod->preu . '€</p>';
                echo '</article>';
                echo '</div></a>';
            }
        }
    }
    ?>
    <footer id="foot">
        <div class="divFoto colorLogo">
            <img src="../img/logo.png">
        </div>
        <div class="infoot">
            <div>
                <p><b>EES-About us</b></p>
                <p>Trinitat Nova 118</p>
                <p>20115 Astigarraga</p>
                <p>(Guipúzcoa)</p>
                <p>Tel: 943 337 150</p>
                <p>Email: contacto@ees.es</p>
                <p>Coordenadas GPS:</p>
                <p>N 43º 16' 54,7" W 1º 57' 12,9"</p>
            </div>
            <div class="rrssfoot">
                <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
                <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
                <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
                <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
            </div>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.6496550056736!2d2.1820199514393566!3d41.44682297915704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bd1df53021b7%3A0xdbdc230ea76f205a!2sVia%20Fav%C3%A8ncia%20-%20Trinitat%20Nova!5e0!3m2!1ses!2ses!4v1634896795493!5m2!1ses!2ses"
                style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </footer>

</body>

</html>
