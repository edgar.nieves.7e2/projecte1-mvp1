<html>

<head>
    <meta charset="UTF-8">
    <title>PRODUCTO</title>
    <link rel="stylesheet" href="../css/cesta.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
</head>
<body>
    <script>
        function cambio() {
        var otro = document.getElementById('prods');
        if (otro.style.display == 'block') {
            otro.style.display = 'none';
        } else {
            otro.style.display = 'block';
        }
    }
</script>
<p id='cosa' class="cesta">
    <img class="fotoC" src="../img/cesta.png" onclick="cambio()">
</p>
<div id="prods">

<?php
    if(isset($_GET['cesta'])){
        echo '<script>cambio();</script>';
    }
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);error_reporting(E_ALL);
    echo '<div class="pedir"><div>';
    echo '<h4>CARRITO</h4>';
    
    
    
    if(isset($_SESSION['cistella'])){
        $laMevaCistella = unserialize($_SESSION['cistella']);
        if ($laMevaCistella->getNumProductes() > 0){
            echo '<p class="total">Productos: ' . $laMevaCistella->getNumProductes() . '</p></div>';
            echo '<div><a href="checkout.php"><button class="vacio">Tramitar pedido</button></a></div></div>';
            echo '<div class="hola" style="overflow-y: auto;">';
            foreach($laMevaCistella->productes as $prod){
                echo '    <div class="producto">';
                echo '        <img class="fcarrito" src="' . $prod->fotos[0] . '">';
                echo '        <h5>' . $prod->titol . '</h5>';
                echo '        <p>' . $prod->preu . '</p>';
                echo '        <div class="cantidad">';
                echo '            <a href="canviQuant.php?id=' . $prod->id . '&canvi=1&page=' . $page . '"><button>-</button></a>';
                echo '            <p class="numero">' . $prod->quantitat . '</p>';
                echo '            <a href="canviQuant.php?id=' . $prod->id . '&canvi=2&page=' . $page . '"><button>+</button></a>';
                echo '        </div>';
                echo '        <div>';
                echo '            <a href="borrarProd.php?id=' . $prod->id . '"><button>X</button></a>';
                echo '        </div>';
                echo '    </div>';  
            }
                echo '</div>';
                echo '  <div class="final">';
                echo '      <p class="precio">Total: ' . $laMevaCistella->getTotal() . '€</p>';
                if(isset($_GET['id'])){
                    echo '      <a href="borrarCesta.php?id=' . $id . '"><button class="vacio">VACIAR CARRITO</button></a>';
                } else {
                    echo '      <a href="borrarCesta.php"><button class="vacio">VACIAR CARRITO</button></a>';
                }
                echo '  </div>';
        } else {
            echo '</div></div>';
            echo '<h2>No hi han productes</h2>';
        }
        $_SESSION['cistella'] = serialize($laMevaCistella);
    } else {
        echo '</div></div>';
        echo '<h2>No hi han productes</h2>';
    }
    ?>
</div>
</body>
</html>

