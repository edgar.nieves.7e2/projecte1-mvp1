<?php
    session_start();
    include('prod.php');
    $id = $_GET['id'];
    $canvi = $_GET['canvi'];
    $page = $_GET['page'];

    if(isset($_SESSION['cistella'])){
        $laMevaCistella = unserialize($_SESSION['cistella']);
    }
    $prod = $laMevaCistella->getProducteById($id);
    if($prod->quantitat < $prod->stoc){
        if($canvi == 2){
            $prod->quantitat++;
        } else if ($prod->quantitat > 1){
            $prod->quantitat--;
        }
    }
    $_SESSION['cistella'] = serialize($laMevaCistella);

    if($page == 1){
        echo "<script>location.href='productos.php?cesta=1'</script>";
    } else {
        echo "<script>location.href='producto.php?id=".$id."&cesta=1'</script>";
    }