<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=, initial-scale=1.0">
  <title>FACTURA</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    thead{
  text-align: left;
}
tbody td{
  border: solid;
  border-width: 0px 1px 0px 0px;
  padding: 0px 10px;
}
tbody td + td + td <?php if(isset($corr2)){echo '+td+td+td+td+td';}?>{
  border-width: 0px;
}
thead th <?php if(isset($corr2)){echo ', thead td';}?> {
  border: solid;
  border-width: 0px 1px 1px 0px;
  padding: 0px 10px;
}
thead th + th + th <?php if(isset($corr2)){echo '+td+th+th+th+th';}?> {
  border-width: 0px 0px 1px;
}
table{
  width: auto;
}
.productos tbody td, .productos thead th{
  border-bottom-width: 1px;
}
.productos tbody td{
  padding: 10px;
}
.productos tfoot td{
  text-align: center;
}
  </style>
</head>

<body>
  <?php
  if(isset($corr2)){

  }else{
  session_start();
  include('prod.php');
  }
  if(isset($_SESSION['cistella'])){
    $laMevaCistella = unserialize($_SESSION['cistella']);
  }

  ?>
    <h2>Factura</h2>
    <hr>
        <p>Trinitat Nova 118</p>
        <p>20115 Astigarraga</p>
        <p>(Guipúzcoa)</p>
      <hr>
    <table>
        <thead>
          <tr>
            <th>Facturar a</th>
            <th>Enviar a</th>
            <th>N° de factura</th>
            <td>103</td>
            <?php
            if(isset($corr2)){
              echo '<th>País</th>';
              echo '<th>Província</th>';
              echo '<th>C.P.</th>';
              echo '<th>Pago</th>';
            }
          ?>
          </tr>
        </thead>
        <tbody>
          <?php
            echo '<tr>
            <td> ' . $nombre . ' ' . $apellido . ' </td>
            <td>' . $direccion1 . '<br>
            ' . $direccion2 . '<br>
            ' . $email . '</td>
            <td><b>Fecha<b></td>
            <td>' . date('d/m/Y') . '</td>';
            if(isset($corr2)){
              echo '<td>' . $pais . '</td>';
              echo '<td>' . $prov . '</td>';
              echo '<td>' . $codPos . '</td>';
              echo '<td>' . $pago . '</td>';
            }            

          '</tr>';
          ?>
        </tbody>
      </table>

      <hr>
      <table class='productos'>
        <thead>
          <tr>
            <th>Cant.</th>
            <th>Descripcion</th>
            <th>Precio Unitario</th>
            <th>Importe</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($laMevaCistella->productes as $prod){
            echo '<tr>
            <td>' . $prod->quantitat . '</td>
            <td>' . $prod->titol . '</td>
            <td>' . $prod->preu . '€</td>
            <td>' . ($prod->preu * $prod->quantitat) . '€</td>
          </tr>';
          }
          ?>
        </tbody>
        <tfoot>
          <tr>
            <th></th>
            <th></th>
            <?php
            echo '<th>Total Factura</th>
            <td>' . $laMevaCistella->getTotal() . '€</td>';
            ?>
          </tr>
        </tfoot>
      </table>

      <hr>
  </div>
</body>
</html>