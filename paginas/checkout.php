<html>

<head>
    <meta charset="UTF-8">
    <title>PRODUCTOS</title>
    <meta name="viewport" content=" initial-scale=1, maximum-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/checkout.css">
</head>

<body>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <div class="rrss">
        <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
        <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
        <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
        <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
    </div>
    <header>
        <a href="../index.html"><img class="logo" src="../img/logo.png"></a>
        <nav>
            <ul>
                <li><a href="../paginas/productos.php">Productos</a></li>
                <li><a href="#foot">Contacto</a></li>
                <li><a href="../index.html#not">Noticias</a></li>
            </ul>
        </nav>
        <div class="topnav" id="myTopnav">
            <a href="productos.php" class="active">PRODUCTOS</a>
            <a href="#foot">CONTACTO</a>
            <a href="../index.html#not">NOTICIAS</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
              <i class="fa fa-bars"></i>
            </a>
        </div>

        <script>
            function myFunction() {
              var x = document.getElementById("myTopnav");
              if (x.className === "topnav") {
                x.className += " responsive";
              } else {
                x.className = "topnav";
              }
            }
        </script>
    </header>
    <?php
    session_start();
    include('prod.php');
    ?>
    <div class="container-fluid cont">
    <div class="row">
        <div class="col-12 col-md-6">
            <h1>Tramitacion del pedido</h1>
            <form action="correo.php" method="post" class="container col-12">
                <div class="row">
                    <div class="offset-0 col-6 offset-md-2 col-md-4">
                        <label for="fname">Nombre:</label><br>
                        <input class="col-12" type="text" id="fname" name="fname" minlength="3" required><br>
                    </div>
                    <div class="offset-0 col-6 col-md-4">
                        <label for="lname">Apellido:</label><br>
                        <input class="col-12" type="text" id="lname" name="lname" minlength="3" required><br><br>
                    </div>
                    <div class="offset-0 col-12 offset-md-2 col-md-8">
                        <label for="email">Email:</label><br>
                        <input class="col-12" type="email" id="email" name="email" required><br><br>
                    </div>
                    <div class="offset-0 col-12 offset-md-2 col-md-8">
                        <label for="adr">Dirección:</label><br>
                        <input class="col-12" type="text" id="adr" name="adr" minlength="5" required><br><br>
                    </div>
                    <div class="offset-0 col-12 offset-md-2 col-md-8">
                        <label for="adr2">Dirección 2:</label><br>
                        <input class="col-12" type="text" id="adr2" name="adr2" minlength="5" required><br><br>
                    </div>
                    <div class="offset-0 col-6 offset-md-2 col-md-4">
                        <label for="pais">País:</label><br>
                        <select class="col-12" name="pais" id="pais" required>
                            <option value="España">España</option>
                            <option value="Francia">França</option>
                            <option value="Portugal">Portugal</option>
                        </select>
                    </div>
                    <div class="offset-0 col-6 col-md-4">
                        <label for="prov">Provincia:</label><br>
                        <select class="col-12" name="prov" id="prov" required>
                            <option value="Lleida">Lleida</option>
                            <option value="Tarragona">Tarragona</option>
                            <option value="Barcelona">Barcelona</option>
                            <option value="Girona">Girona</option>
                        </select>
                    </div>
                    <div class="offset-0 col-6 offset-md-2 col-md-4">
                        <br>
                        <label for="cpostal">Código postal:</label>
                        <input class="col-12" type="text" id="cpostal" name="cpostal" minlength="3" required>
                    </div>
                    <div class="col-6 col-md-4"></div>
                    <div class="offset-0 col-7 offset-md-2 col-md-5">
                        <br>
                        <label for="pago">Pago:</label><br>
                        <input class="ml-3" type="radio" id="cr" name="pago" value="Contra reembolso" required>
                        <label for="cr">Contra reembolso</label><br>
                        <input class="ml-3" type="radio" id="bitcoin" name="pago" value="bitcoin">
                        <label for="bitcoin">Bitcoin</label> 
                        <br><br>
                    </div>
                    <div class="offset-0 col-6 offset-md-2 col-md-4">
                        <input class="col-12 mb-5" type="submit" value="Finalizar">
                    </div>
                </div>
            </form> 
        </div>
    
        <div class="col-12 col-md-5">
            <h1>Cesta</h1>
            <?php
                ini_set('display_errors', 1);
                ini_set('display_startup_errors', 1);error_reporting(E_ALL);
                if(isset($_SESSION['cistella'])){
                    $laMevaCistella = unserialize($_SESSION['cistella']);
                    if ($laMevaCistella->getNumProductes() > 0){
                        foreach($laMevaCistella->productes as $prod){
                            echo '<div class="articulos">';
                            echo '<article>';
                            echo '<img style="width:25%; float: left; margin-right: 20px;" src="' . $prod->fotos[0] . '"/>';
                            echo '<h6>' . $prod->categoria . '</h6>';
                            echo '<h4>' . $prod->titol . '</h4>';
                            echo '<img style="width: 100px" class="estrella" src="../img/estrella.png">';
                            echo '<div class="preuQuant"><p>' . $prod->preu . '€</p>';
                            echo '<p>x ' . $prod->quantitat . '</p></div>';
                            echo '</article>';
                            echo '</div>';
                        }     
                    }
                    $_SESSION['cistella'] = serialize($laMevaCistella);
                } else {
                    echo '<h2>No hi han productes</h2>';
                }
            ?>
        </div>
    </div>
    </div>
    
    
    <footer id="foot">
        <div class="divFoto colorLogo">
            <img src="../img/logo.png">
        </div>
        <div class="infoot">
            <div>
                <p><b>EES-About us</b></p>
                <p>Trinitat Nova 118</p>
                <p>20115 Astigarraga</p>
                <p>(Guipúzcoa)</p>
                <p>Tel: 943 337 150</p>
                <p>Email: contacto@ees.es</p>
                <p>Coordenadas GPS:</p>
                <p>N 43º 16' 54,7" W 1º 57' 12,9"</p>
            </div>
            <div class="rrssfoot">
                <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
                <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
                <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
                <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
            </div>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.6496550056736!2d2.1820199514393566!3d41.44682297915704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bd1df53021b7%3A0xdbdc230ea76f205a!2sVia%20Fav%C3%A8ncia%20-%20Trinitat%20Nova!5e0!3m2!1ses!2ses!4v1634896795493!5m2!1ses!2ses"
                style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </footer>

</body>

</html>
