<?php

class Producte{
    public $id;
    static $contador = 1;
    public $titol;
    public $categoria;
    public $preu;
    public $descripció;
    public $colors = [];
    public $stoc;
    public $caracteristiques;
    public $especificacions;
    public $instruccions;
    public $quantitat; 
    public $fotos = [];

    function __construct(){
        $this->id = $this::$contador;
        $this::$contador++;
    }

    function getId(){
        return $this->id;
    }

    function addFoto($foto){
        $this->fotos[] = $foto;
    }

    function toArray(){
        $tmpArray = array(
            "titol"=>$this->titol,
            "categoria"=>$this->categoria,
            "preu"=>$this->preu,
            "descripcio"=>$this->descripció,
            "colors"=>isset($this->colors[0]) ? $this->colors[0] : NULL,
            "stoc"=>$this->stoc,
            "caracteristiques"=>$this->caracteristiques,
            "especificacions"=>$this->especificacions,
            "instruccions"=>$this->instruccions,
            "quantitat"=>$this->quantitat,
            "fotos"=>isset($this->fotos[0]) ? $this->fotos[0] : NULL
          );
          return $tmpArray;
      }

        

}

class Cataleg{
    public $productes = [];

    function addProducte($prod){
        $this->productes[] = $prod;
    }

    function getProducteById($id){
        foreach($this->productes as $prod){
            if ($prod->id == $id){
                return $prod;
            }
        }
        return "No existeix aquest producte";
    }

    function llistar(){
        foreach($this->productes as $prod){
            print_r($prod);
        }
    }
}

class Cistella{
    public $productes = [];
    public $count;

    function addProducte($prod){
        $exists = false;
        foreach($this->productes as $producto){
            if($prod->id == $producto->id){
                if($producto->quantitat < $producto->stoc){
                    $producto->quantitat++;
                }
                $exists = true;
            }
        }
        if(!$exists){
            $prod->quantitat++;
            $this->productes[] = $prod;
        }
    }

    function deleteProducte($id){
        $cont = 0;
        foreach($this->productes as $prod){
            if ($prod->id == $id){
                $prod->quantitat = 0;
                unset($this->productes[$cont]);
            }
            $cont++;
        }
    }

    function llistar(){
        foreach($this->productes as $prod){
            print_r($prod);
        }
    }

    function getTotal(){
        $total = 0;
        foreach($this->productes as $prod){
            $total += ($prod->preu * $prod->quantitat);
        }
        return $total;
    }

    function getNumProductes(){
        return sizeof($this->productes);
    }

    function getProducteById($id){
        foreach($this->productes as $prod){
            if ($prod->id == $id){
                return $prod;
            }
        }
        return "No existeix aquest producte";
    }

    function buidar(){
        foreach($this->productes as $prod){
            $prod->quantitat = 0;
        }
        $this->productes = array();
    }
}
