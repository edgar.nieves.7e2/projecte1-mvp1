<html>

<head>
    <meta charset="UTF-8">
    <title>PRODUCTO</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="../css/producto.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="rrss">
        <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
        <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
        <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
        <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
    </div>
    <header>
        <a href="../index.html"><img class="logo" src="../img/logo.png"></a>
        <nav>
            <ul>
                <li><a href="../paginas/productos.php">Productos</a></li>
                <li><a href="#foot">Contacto</a></li>
                <li><a href="../index.html#not">Noticias</a></li>
            </ul>
        </nav>
        <div class="topnav" id="myTopnav">
            <a class="active">PRODUCTO</a>
            <a href="productos.php" class="active">PRODUCTOS</a>
            <a href="#foot">CONTACTO</a>
            <a href="../index.html#not">NOTICIAS</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
              <i class="fa fa-bars"></i>
            </a>
        </div>

        <script>
            function myFunction() {
              var x = document.getElementById("myTopnav");
              if (x.className === "topnav") {
                x.className += " responsive";
              } else {
                x.className = "topnav";
              }
            }
        </script>
    </header>
     
    <script type='text/javascript'>
        function changeImage(a) {
            document.getElementById('img').src=a.src;
        }
    </script>
    <?php
    $page = 2;
    //-1) Inicio el debug dels errors
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);error_reporting(E_ALL);
        session_start();
        include('prod.php');
        $id = $_GET["id"];
        include('cesta.php');
    
        
        $prod = $elMeuCataleg->getProducteById($id);
        echo "<div class='titulo'>";
        echo "<div>
        <img class='categoria' src='../img/estrella.png'>
        <p>" . $prod->categoria . "</p>
    </div>";
        echo "<div>
        <h2>" . $prod->titol . "</h2>
    </div></div>";

        echo "<div class='articulo'>";
        echo "<div class='div1'>
        <img id='img' class='foto' src=" . $prod->fotos[0] . ">
    </div>";
        echo "<div>
        <h3 class='presio'>En stock: " . $prod->stoc . "</h3>
        <p class='preu'>" . $prod->preu . "€</p>
    </div>";
    echo "<div class='fotos'>";
    $cont = 0;
    foreach($prod->fotos as $foto){
        if ($cont == 4){
            break;
        }else{
            echo "<img src='" . $foto . "' onclick='changeImage(this);'>";            
        }
        $cont++;
    }
    echo "</div>";
    echo "<a href='addProducto.php?id=".$id."'><button class='addCesta'>Añadir a la cesta</button></a>";
        echo "<div class='h3'>
        <h3 >Descripción</h3>
        <p>" . $prod->descripció . "</p>
    </div> </div>";
        echo "<div class='divFoto'>
        <img src='../img/logo.png'>
    </div>";
        echo "<div class='articulo'>
        <h3>Caracteristicas</h3>
        <p class='borde'>" . $prod->caracteristiques . "</p>";
        echo "<h3>Especificaciones</h3>
        <p class='borde'>" . $prod->especificacions . "</p>
        ";
        echo "<h3>Instrucciones</h3>
        <p class='borde'>" . $prod->instruccions . "</p>
    </div>";
    ?>
    <footer id="foot">
        <div class="divFoto colorLogo">
            <img src="../img/logo.png">
        </div>
        <div class="infoot">
            <div>
                <p><b>EES-About us</b></p>
                <p>Trinitat Nova 118</p>
                <p>20115 Astigarraga</p>
                <p>(Guipúzcoa)</p>
                <p>Tel: 943 337 150</p>
                <p>Email: contacto@ees.es</p>
                <p>Coordenadas GPS:</p>
                <p>N 43º 16' 54,7" W 1º 57' 12,9"</p>
            </div>
            <div class="rrssfoot">
                <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
                <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
                <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
                <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
            </div>
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2990.6496550056736!2d2.1820199514393566!3d41.44682297915704!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4bd1df53021b7%3A0xdbdc230ea76f205a!2sVia%20Fav%C3%A8ncia%20-%20Trinitat%20Nova!5e0!3m2!1ses!2ses!4v1634896795493!5m2!1ses!2ses"
                style="border:0;" allowfullscreen="" loading="lazy"></iframe>
    </footer>

</body>

</html>
