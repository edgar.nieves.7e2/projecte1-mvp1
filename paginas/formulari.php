<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="../img/logo.png" type="image/icon type">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../css/formulari.css">
    <title>Añadir producto</title>
</head>
<body>
    <h5>Añadir producto</h5>
    
    <?php
    if(isset($_GET['id'])){
        echo "<form action='./DB_editar.php' method='post'>";
    }else{
        echo "<form action='./DB_insert.php' method='post'>";   
    }
        ?>
        <div class="row">
        <div class="form-group col-11">
            <label for="titulo">Título: </label>
            <input type="text" class="form-control" id="titulo" aria-describedby="titulo"
                placeholder="Introduce el nombre del producto" name="titol">
        </div>

        <div class="form-group col-1">
            <label for="id">ID</label>
            <input type="text" name="id" class="form-control" value="" id="id" aria-describedby="titulo"
            readonly>
        </div>
        </div>

        <div class="form-group">
            <label for="categoria">Categoria:</label>
            <select class="form-control select2bs4" aria-label="Default select example" id="categoria" name="categoria">
                <option selected value="Auriculares">Auriculares</option>
                <option value="Portatiles">Portatiles</option>
                <option value="Relojes">Relojes</option>
                <option value="SmartTV">SmartTV</option>
                <option value="Smartphone">Smartphone</option>
            </select>
        </div>

            <div class="form-group">
                <label for="precio">Precio: </label>
                <input type="number" class="form-control" id="precio" aria-describedby="titulo"
                    placeholder="Introduce el precio del producto" value="" name="preu">
            </div>

            <div class="form-group">
                <label for="descripcion">Descripción: </label>
                <textarea class="form-control" rows="3" id="descripcion" aria-describedby="titulo"
                    placeholder="Introduce una descripcion para el producto" value="" name="descripcio"></textarea>
            </div>

            <div class="form-group">
                <label for="stock">Stock: </label>
                <input type="number" class="form-control" id="stock" aria-describedby="titulo"
                    placeholder="Introduce el stock del producto" value="" name="stoc">
            </div>
            <div class="form-group">
                <label for="especificacions">Especificaciones: </label>
                <textarea class="form-control" rows="3" id="especificacions" aria-describedby="titulo"
                    placeholder="Introduce especificaciones para el producto" value="" name="especificacions"></textarea>
            </div>

            <div class="form-group">
                <label for="instruccions">Instrucciones: </label>
                <textarea class="form-control" rows="3" id="instruccions" aria-describedby="titulo"
                    placeholder="Introduce instrucciones para el producto" value="" name="instruccions"></textarea>
            </div>

            <div class="form-group">
                <label for="caracteristiques">Características: </label>
                <textarea class="form-control" rows="3" id="caracteristiques" aria-describedby="titulo"
                    placeholder="Introduce caracteristicas para el producto" value="" name="caracteristiques"></textarea>
            </div>
            
            
                       
         <?php
        
    if(isset($_GET['id'])){
        echo "<div class='centro'><input type='submit' value='Guardar'></div>";
        $id = $_GET['id'];
        include("DB_connexio.php");
        include("model.php");
        $prod = $connexio->prepare("SELECT * FROM productes WHERE id=$id");
        $prod->setFetchMode(PDO::FETCH_CLASS , 'Producte');
        $prod->execute();
        $product = $prod->fetch();

        echo "<script>";
        echo "document.getElementById('titulo').value = '" . $product->titol ."';";
        echo " document.getElementById('id').value = '" . $product->id ."';";
        echo " document.getElementById('categoria').value = '" . $product->categoria ."';";
        echo " document.getElementById('precio').value = '" . $product->preu ."';";
        echo " document.getElementById('descripcion').value = '" . $product->descripcio ."';";
        echo " document.getElementById('stock').value = '" . $product->stoc ."';";
        echo " document.getElementById('caracteristiques').value = '" . $product->caracteristiques ."';";
        echo " document.getElementById('especificacions').value = '" . $product->especificacions ."';";
        echo " document.getElementById('instruccions').value = '" . $product->instruccions ."';";
        echo "</script>";
    } else {
        echo "<div class='centro'><input type='submit' value='Añadir'></div>";
    }
    echo "
        <div class='centro'>
            <a href='listaAdmin.php'>Cancelar</a>
        </div>
    </form>";
    ?> 
    
</body>

</html>