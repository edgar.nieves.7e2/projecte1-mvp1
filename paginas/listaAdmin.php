<?php
session_start();
$_SESSION['id_usuari'] = $_REQUEST['usuari'];
$_SESSION['clau_acces'] = $_REQUEST['contrasenya'];

?>
<html>
<head>
    <meta charset="UTF-8">
    <title>PRODUCTO</title>
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <link rel="icon" href="../img/logo.png" type="image/icon type">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../css/listaAdmin.css">
</head>

<body>
<?php   
       if($_SESSION['id_usuari'] !== "admin" || $_SESSION['clau_acces'] !== "admin"){
        echo "<br><br><p>Identitat incorrecte";
        header("location: login.html");
    }
   ?> 
    <div class="rrss">
        <a href="https://web.whatsapp.com/"><img class="icono" src="../img/wicon.png"></a>
        <a href="https://www.instagram.com/"><img class="icono" src="../img/igicon.png"></a>
        <a href="https://twitter.com/"><img class="icono" src="../img/ticon.png"></a>
        <a href="https://ca-es.facebook.com/"><img class="icono" src="../img/ficon.png"></a>
    </div>
    <header>
        <a href="../index.html"><img class="logo" src="../img/logo.png"></a>
        <nav>
            <ul>
                <li><a href="../paginas/productos.php">Productos</a></li>
                <li><a href="#foot">Contacto</a></li>
                <li><a href="../index.html#not">Noticias</a></li>
            </ul>
        </nav>
        <div class="topnav" id="myTopnav">
            <a href="productos.php" class="active">PRODUCTOS</a>
            <a href="#foot">CONTACTO</a>
            <a href="../index.html#not">NOTICIAS</a>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
              <i class="fa fa-bars"></i>
            </a>
        </div>

        <script>
      function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
          x.className += " responsive";
        } else {
          x.className = "topnav";
        }
      }
    </script>
    </header>

    <div class="container-fluid my-5">
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <h1 class='d-flex justify-content-center'>Listado de productos</h1>
            </div>
            <div class="col-3 pt-2">
                <a class="btn addProd" href="formulari.php" role="button">(+)Añadir producto</a>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-12">
                <table class='table'>
                    <tr>
                        <th class="col-1">ID#</th>
                        <th class="col-5 text-left">Nombre</th>
                        <th class="col-1">Stock</th>
                        <th class="col-2">Categoria</th>
                        <th class="col-1">Precio</th>
                        <th class="col-2">Acciones</th>
                    </tr>

    <?php
        include("DB_connexio.php");
        include("model.php");
        $productos = $connexio->prepare("SELECT * FROM productes");
        $productos->setFetchMode(PDO::FETCH_CLASS , 'Producte');
        $productos->execute();
        while($product = $productos->fetch()){
            echo "<tr><td>" . $product->id . "</td>";
            echo "<td>" . $product->titol . "</td>";
            echo "<td>" . $product->stoc . "</td>";
            echo "<td>" . $product->categoria . "</td>";
            echo "<td class='precio'>" . $product->preu . " €</td>";
            echo "<td class='accions'><a href='formulari.php?id=" . $product->id . "'><img class='mod' src='../img/edit.png'></a> | <a href='DB_eliminar.php?id=" . $product->id . "'><img class='mod' src='../img/delete.png'></a></td></tr>";
        }
    ?>
                </table>
            </div>
        </div>
    </div> 
    
    
</body>
</html>